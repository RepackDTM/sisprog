#include "triangle.h"
#include <iostream>
#include <cmath>

triangle::triangle() {
    this->a = 0;
    this->b = 0;
    this->c = 0;
}

triangle::triangle(double p_a = 0, double p_b = 0, double p_c = 0) {
    this->a = p_a;
    this->b = p_b;
    this->c = p_c;
}
bool triangle::exst_tr(){
    if (a + b <= c || a + c <= b || b + c <= a)
        return false;
    else
        return true;
}
void triangle::set(double p_a = 0, double p_b = 0, double p_c = 0){
    this->a = p_a;
    this->b = p_b;
    this->c = p_c;
}
void triangle::show(){
    std::cout << a << std::endl;
    std::cout << b << std::endl;
    std::cout << c << std::endl;
}
double triangle::perimetr(){
    return a + b + c;
}
double triangle::square(){
    double p = 0.5 * perimetr();
    return sqrt(p * (p - a) * (p - b) * (p - c));
}
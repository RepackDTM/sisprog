#include "circle.h"
#define _USE_MATH_DEFINES
#include <cmath>

circle::circle(float r, float x, float y) {
	radius = r;
	x_center = x;
	y_center = y;
}
void circle::set_circle(float r, float x, float y) {
	radius = r;
	x_center = x;
	y_center = y;
}
float circle::square() {
	return M_PI * radius * radius;
}
bool circle::triangle_around(float a, float b, float c) {
	float p = 0.5 * (a + b + c);
	float in_r = sqrt((p - a) * (p - b) * (p - c) / p);
	if(fabs(in_r - radius) <= 0.001)
		return true;
	else
		return false;
}
bool circle::triangle_in(float a, float b, float c) {
	float p = 0.5 * (a + b + c);
	float s = sqrt(p * (p - a) * (p - b) * (p - c));
	float out_r = a * b * c / (4 * s);
	if(fabs(out_r - radius) <= 0.001)
		return true;
	else
		return false;
}
bool circle::check_circle(float r, float x_cntr, float y_cntr) {
	float interval = sqrt(powf(x_cntr - this->x_center, 2) + pow(y_cntr - this->y_center, 2));
	if(r + radius >= interval)
		return true;
	else
		return false;
}